#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Bjornars'
    ###################################
    #neural network - Backpropagation##
    ###################################
import theano
from theano import tensor as T
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
import numpy as np
from Connection import Connection
import requests
import sys
import pickle
import math
import time
srng = RandomStreams()

def dropout(X, p=0.):
    if p > 0:
        retain_prob = 1 - p
        X *= srng.binomial(X.shape, p=retain_prob, dtype=theano.config.floatX)
        X /= retain_prob
    return X

def elliot(x,W):
    return 1/2((1+x)/(1+math.fabs(x)))

def sigmoid(X,W):
    return T.nnet.sigmoid(T.dot(X, W))

def sigmoidB(W,x,bias):
    return T.nnet.sigmoid(T.dot(W, x) + bias)

def rectify(X):
    return T.maximum(X, 0.)
    # To fix so you do not get infinity numbers
def softmax(X):
    e_x = T.exp(X - X.max(axis=1).dimshuffle(0, 'x'))
    return e_x / e_x.sum(axis=1).dimshuffle(0, 'x')

def floatX(X):
    return np.asarray(X, dtype=theano.config.floatX)

def init_weights(shape):
    return theano.shared(floatX(np.random.randn(*shape) * 0.01))
class Network():
    def __init__(self,syssarn):
        #self.path = Loader('media\datasets\mnist')
        # size = i.e [784,30,10] which corresponds to number of neurons
        asd=0


        self.size = []
        for i in range(1,len(syssarn)):
            self.size.append(int(syssarn[i]))
        X = T.fmatrix()
        Y = T.fmatrix()

        #This to change number of layers 1, 2 ,3 4 etc
        self.numLayers = len(self.size)
        self.weightMatrix=[]
        self.initilizeMatrix()
        #was 0.2, 0.5
        noise = self.model(X,self.weightMatrix,self.numLayers,0.2,0.5)
        py_x = self.model(X,self.weightMatrix,self.numLayers,0.,0.)
        y_x = T.argmax(py_x,axis=1)

        self.cost = T.mean(T.nnet.categorical_crossentropy(noise,Y))

        updates = self.RMSprop(self.cost,self.weightMatrix)



        self.train = theano.function(inputs=[X, Y], outputs=self.cost, updates=updates, allow_input_downcast=True, mode="FAST_RUN")
        self.predict = theano.function(inputs=[X], outputs=py_x, allow_input_downcast=True)
        self.game = Connection()
        #trX,trX2,trX3,trX4,self.trY = self.game.mnist()
        trX, self.trY = self.game.getPreProssedMnist()
        #teX,teY = load_mnist(dataset="testing")

        accuracy=0
        epocs = 0
        while(epocs<1):
            i = 0
            for start, end in zip(range(0, len(trX), 128), range(128, len(trX), 128)):
                cost = self.train([np.reshape(tr, (8)) for tr in trX[start:end]], self.trY[start:end])
                #cost = self.train([np.reshape(tr, (16)) for tr in trX2[start:end]], self.rotateMove(self.trY[start:end],1))
                #cost = self.train([np.reshape(tr, (16)) for tr in trX3[start:end]], self.rotateMove(self.trY[start:end],2))
                #cost = self.train([np.reshape(tr, (16)) for tr in trX4[start:end]], self.rotateMove(self.trY[start:end],3))
            #print(self.predict(trX))
            epocs+=1
        #for i in range(len(predict(trX))):
        #    print(predict(trX)[i])

    def playOneGame(self):
           a,b = self.playGame(False,True)
    def start(self):
        randomPlayer = []
        neuralPer = []
        neuralPlayer = []




        for i in range(50):
            a, b =self.playGame(False,False)
            neuralPlayer.append(a)
            neuralPer.append(b)

        for i in range(50):
            a,b = self.playGame(True,False)
            randomPlayer.append(a)

        randomScore = 0
        neuralScore = 0
        neuralWins = 0

        print("Random:")
        for i in range(50):
            randomScore+= randomPlayer[i]
            neuralScore+=neuralPlayer[i]

            if randomPlayer[i] < neuralPlayer[i]:
                neuralWins += 1
            print(randomPlayer[i])

        print("Neural:")
        for i in range(50):
            print(neuralPlayer[i], "Percent random moves: ", neuralPer[i])

        print("50 random runs total score: ", randomScore, " 50 neural runs total score: ", neuralScore)
        print("random run avarage score: " ,(randomScore/50), " 50 neural avarage score: ", (neuralScore/50))
        print("Neural wins: ", neuralWins, " in percent: ", (neuralWins/50)*100)

        print(self.welch(randomPlayer,neuralPlayer))

    def playGame(self, randomPlayer, printBoard):

        self.game.initGame()
        self.game.move('0')

        board = []
        run = True
        lastBoard = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
        randomMoves = 0
        neuralMoves= 0
        lastPredictedMoves = []
        predictedMoves = []
        bestMove = -1
        moves = 0
        equalBoard = 0
        while(run):
            lastBoard = board
            board = self.game.getBoard()
            equal = True
            for i in range(4):
                if moves == 0:
                    equal = False
                    break
                for j in range(4):
                    if lastBoard[i][j] != board[i][j]:
                        equal = False
                        break

            move = -1
            if randomPlayer:
                time.sleep(0.03)
                move = self.game.randomplayer()
                randomMoves += 1
               # print("RANDOM")
            elif equal:
                equalBoard+=1
                move = self.findNBestMove(predictedMoves,equalBoard)
            else:
               # print("NOT RANDOM")
                equalBoard = 0
                lastPredictedMoves = predictedMoves
                bestMove, predictedMoves = self.predictMove(board)
                #print(bestMove)
                move = predictedMoves[bestMove]
                neuralMoves+=1
            if printBoard:
                if move == 0:
                    print("Move: up")
                elif move == 1:
                    print("Move: right")
                elif move == 2:
                    print("Move: down")
                elif move == 3:
                    print("Move: left")

                print(self.game.getSquareBoard())
            self.game.move(str(move))
            if self.game.isGameOver():
                print("GAME OVER"," Random Moves in %: ", (randomMoves/(randomMoves+neuralMoves))*100)
                print(self.game.getSquareBoard())
                run = False
                return self.getHighestTile(self.game.getBoard()),(randomMoves/(randomMoves+neuralMoves))*100

            moves += 1

    def predictMove(self, board):
        currentBest = np.array([0.,0.,0.,0.])
        arraylist1,arraylist2,arraylist3,arraylist4 = self.game.getPreProssed(board)

        allMatrixes = []
        allMatrixes.append(arraylist1)
        allMatrixes.append(arraylist2)
        allMatrixes.append(arraylist3)
        allMatrixes.append(arraylist4)
        for i in range(4):
            predictMatrix = self.rotateMove(self.predict(allMatrixes[i])[0], i)
            for j in range(4):
                if predictMatrix[j] > currentBest[j]:
                    currentBest[j] = predictMatrix[j]
        bestIndex = np.argmax(currentBest)
        return bestIndex, currentBest



    '''def predictMove(self, board):
        predictedMoves= []
        theanoArray1,theanoArray2,theanoArray3,theanoArray4 = self.game.getTheanoBoard(board)

        highest = 0
        highestIndex = -1

        for i in range(4):
            #predicted = self.rotateMove(self.predict(theanoArray1),i)
            #move = (np.mean(np.argmax(self.trY,i), axis=1) == self.rotateMove(predicted,4))
            predictMatrix = self.predict(theanoArray1)[0]
            highestIndex = T.argmax(predictMatrix, axis=1)
            predicted = self.rotateMove(predicted,i)
            predictedMoves.append(predicted)
            if move>highest:
                highest = move
                highestIndex = i


        for i in range(4):
            if predictedMoves[i] > highest:
                highest = predictedMoves[i]
                highestIndex = i

        return highestIndex, predicted'''

    #finds the n' best move(n = 2 : the 3. best move)

    def findNBestMove(self, array, n):

        if array[0] == 0.25 and array[1] == 0.25 and array[2] == 0.25  and array[3] == 0.25:
            return self.game.randomplayer()

        sortedArray = np.sort(array)
        for i in range(4):
            if array[i] == sortedArray[3-n]:
                return i
        return -1

    def getHighestTile(self,board):
        highest = 0
        for i in range(len(board)):
            for j in range(len(board[i])):
                if highest < board[i][j]:
                    highest = board[i][j]

        return highest

    def rotateMove(self,array, n):
        if n == 0:
            return array
        newArray = [0,0,0,0]
        newArray[0] = array[3]
        newArray[1] = array[0]
        newArray[2] = array[1]
        newArray[3] = array[2]

        newArray[0] = array[3]
        newArray[1] = array[0]
        newArray[2] = array[1]
        newArray[3] = array[2]
        return self.rotateMove(newArray, n-1)




    def blind_test(self,flat_cells):
         #self.path = Loader('media\datasets\mnist')
            # size = i.e [784,30,10] which corresponds to number of neurons
        self.size = []
        self.layers = [0,784,625,625,10]
        for i in range(1,len(self.layers)):
            self.size.append(int(self.layers[i]))
        X = T.fmatrix()
        Y = T.fmatrix()
        #This to change number of layers 1, 2 ,3 4 etc
        self.numLayers = len(self.size)
        self.weightMatrix=[]
        self.initilizeMatrix()

        noise = self.model(X,self.weightMatrix,self.numLayers,0.2,0.5)
        py_x = self.model(X,self.weightMatrix,self.numLayers,0.,0.)
        y_x = T.argmax(py_x,axis=1)
        cost = T.mean(T.nnet.categorical_crossentropy(noise,Y))

        updates = self.RMSprop(cost,self.weightMatrix)

        train = theano.function(inputs=[X, Y], outputs=cost, updates=updates, allow_input_downcast=True, mode="FAST_RUN")
        predict = theano.function(inputs=[X], outputs=y_x, allow_input_downcast=True)

        trX, trY = load_mnist()


        accuracy=0
        epocs = 0
        print("start")
        while(epocs<4):
            i = 0
            for start, end in zip(range(0, len(trX), 128), range(128, len(trX), 128)):

                cost = train([np.reshape(tr, (784)) for tr in trX[start:end]], trY[start:end])
            print(i)
            epocs+=1


        print (predict(flat_cells))
        return



    def initilizeMatrix(self):
        for i in range(self.numLayers-1):
            self.weightMatrix.append(init_weights((self.size[i],self.size[i+1])))


    def model(self,X, weightmatrix,numberofhidden, p_drop_input, p_drop_hidden):

        xValues =[]
        hidden=[]
        xValues.append(dropout(X,p_drop_input))
        hidden.append(rectify(T.dot(X,weightmatrix[0])))
        #hidden.append(sigmoid(X, weightmatrix[0]))


        if(numberofhidden-1)!= 1:
            for i in range(1,numberofhidden-2):
                xValues.append(dropout(hidden[i-1],p_drop_hidden))
                hidden.append(rectify(T.dot(hidden[i-1],weightmatrix[i])))
                #hidden.append(sigmoid(hidden[i-1],weightmatrix[i]))

            xValues.append(dropout(hidden[len(hidden)-1],p_drop_hidden))


        return softmax(T.dot(xValues[len(xValues)-1],weightmatrix[len(weightmatrix)-1]))
        #return sigmoid(xValues[len(xValues)-1],weightmatrix[len(weightmatrix)-1])

    def dropout(X, p=0.):
        if p > 0:
            retain_prob = 1 - p
            X *= srng.binomial(X.shape, p=retain_prob, dtype=theano.config.floatX)
            X /= retain_prob
        return X
    #lr=0.001 | beste så langt
    def RMSprop(self,cost, params, lr=0.001, rho=0.9, epsilon=1e-6):
        grads = T.grad(cost=cost, wrt=params)
        updates = []
        for p, g in zip(params, grads):
            acc = theano.shared(p.get_value() * 0.)
            acc_new = rho * acc + (1 - rho) * g ** 2
            gradient_scaling = T.sqrt(acc_new + epsilon)
            g = g / gradient_scaling
            updates.append((acc, acc_new))
            updates.append((p, p - lr * g))
        return updates

    def welch(self,list1, list2):
        params = {"results": str(list1) + " " + str(list2), "raw": "1"}
        resp = requests.post('http://folk.ntnu.no/valerijf/6/', data=params)
        return resp.text