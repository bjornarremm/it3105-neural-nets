import random
import json, requests
import urllib.request
import os
import numpy as np

class Connection():
    sessionId = ''
    jsonObj = None
    fileOpened = False
    file = None
    def getUrl(self):
        return ('http://localhost:8080/hi/state/'+self.sessionId+'/')

    def initGame(self):
        self.sessionId = self.getJson('http://localhost:8080/hi/start/json')['session_id']
        self.jsonObj = self.getGameState()

    def getGameState(self):

        return self.getJson(self.getUrl()+'json')

    def move(self, move):
        self.jsonObj = self.getJson(self.getUrl()+'/move/' + move + '/json')
        self.jsonObj['move'] = move

    def getJson(self,url):
        response = urllib.request.urlopen(url)
        byte = response.read()
        decoded = byte.decode('utf-8')
        return json.loads(decoded)

    def getBoard(self):
        return self.jsonObj['grid']

    def getSquareBoard(self):
        board = self.getBoard()
        ret = ''
        for i in range(len(board)):
            ret += str(board[i]) + '\n'
        return ret



    def randomplayer(self):
        list = ['0','1','2','3']
        return random.choice(list)

    def isGameOver(self):
        return self.jsonObj['over']
    def openFile(self):
        self.file = open('C:/Users/Joakim/Documents/file','w')

    def save(self, input):
        if self.file is None:
            self.openFile()
            #  self.file.write(input+'\n')
            json.dump(input, self.file)
        else:
            self.openFile()
            #     self.file.write(input+'\n')
            json.dump(input, self.file)

    def saveJson(self, jsonList):
        data = json.dumps(jsonList)
        json.dump(data,self.file)

    def load(self):
        with open('C:/Users/Joakim/Documents/file.json', 'r') as data_file:
            stringData = json.load(data_file)
            return stringData
            #board = stringData[0]['grid'][0]
    #print(stringData[0]['grid'][1][1])'

    def one_hot(self,x,n):
        if type(x) == list:
            x = np.array(x)
        x = x.flatten()
        o_h = np.zeros((len(x),n))
        o_h[np.arange(len(x)),x] = 1
        return o_h

    def mnist(self):
        jsonData = self.load()
        arraylist1 = []
        arraylist2 = []
        arraylist3 = []
        arraylist4 = []
        labels = []
        for i in range(len(jsonData)):
            arraylist1.append(np.array(jsonData[i]['grid']).reshape((16)))
            arraylist2.append(np.rot90(np.array(jsonData[i]['grid'])).reshape((16)))
            arraylist3.append(np.rot90(np.array(jsonData[i]['grid']),2).reshape((16)))
            arraylist4.append(np.rot90(np.array(jsonData[i]['grid']),3).reshape((16)))

            temp = [0,0,0,0]
            temp[jsonData[i]['move']] = 1
            labels.append(np.array(temp))


        return arraylist1, arraylist2, arraylist3, arraylist4,labels


    def getPreProssedMnist(self):
        jsonData = self.load()
        array = []
        labels = []
        for i in range(len(jsonData)):
            array.append(np.array(self.getPreProssedBoard(jsonData[i]['grid'])))

            temp = [0,0,0,0]
            temp[jsonData[i]['move']] = 1
            labels.append(np.array(temp))

        return array,labels

    def getTheanoBoard(self, board):
        list1 = []
        list1.append(np.array(board).reshape(16))
        list2 = []
        list2.append(np.rot90(np.array(board)).reshape(16))
        list3 = []
        list3.append(np.rot90(np.array(board),2).reshape(16))
        list4 = []
        list4.append(np.rot90(np.array(board),3).reshape(16))
        return list1,list2,list3,list4

    def getPreProssedBoard(self,board):
        merges = self.countMerges(board)
        merges += self.highestTileInCorner(board)
        returnList1 = np.array(merges)
        return returnList1

    def getPreProssed(self, board):
        merges = self.countMerges(board)
        merges += self.highestTileInCorner(board)
        returnList1 = []
        returnList2 = []
        returnList3 = []
        returnList4 = []

        returnList1.append(np.array(merges))
        returnList2.append(np.array(self.rotatePreprossed(merges,1)))
        returnList3.append(np.array(self.rotatePreprossed(merges,2)))
        returnList4.append(np.array(self.rotatePreprossed(merges,3)))

        return returnList1,returnList2,returnList3,returnList4

    def rotatePreprossed(self,array, n):
        if n == 0:
            return array
        newArray = []
        newArray.append(array[3])
        newArray.append(array[0])
        newArray.append(array[1])
        newArray.append(array[2])

        newArray.append(array[7])
        newArray.append(array[4])
        newArray.append(array[5])
        newArray.append(array[6])
        return self.rotatePreprossed(newArray,n-1)



    def main(self):
        self.initGame()
        run = True
        moves = 0
        self.openFile()
        allMoves = []
        while(run):

            randomNum = self.randomplayer()
            self.move(randomNum)
            moves+= 1
            #asd = 'moves ' + str(moves)+ 'board: ' + str(self.getBoard())
            #self.save(self.jsonObj)
            self.jsonObj['totalMoves'] = moves
            allMoves.append(self.jsonObj)

            print(self.getSquareBoard())
            if self.isGameOver():
                print('Game over')
                self.saveJson(allMoves)
                run = False
                self.load()

    def test(self):
        self.mnist()



    def countMerges(self, board):
        merges = []
        merges.append(self.countMergeUp(board))
        merges.append(self.countMergeRight(board))
        merges.append(self.countMergeDown(board))
        merges.append(self.countMergeLeft(board))

        return merges



    def countMergeUp(self, board):
        merges = 0
        for i in range(4):
            lastValue = -1
            for j in range(4):
                if board[j][i] == 0:
                    continue
                elif board[j][i] == lastValue:
                    merges+=1
                    lastValue = 0
                else:
                    lastValue = board[j][i]

        return merges

    def countMergeDown(self, board):
        merges = 0
        for i in range(4):
            lastValue = -1
            for j in range(3,-1,-1):
                if board[j][i] == 0:
                    continue
                elif board[j][i] == lastValue:
                    merges+=1
                    lastValue = 0
                else:
                    lastValue = board[j][i]

        return merges

    def countMergeLeft(self,board):
        merges = 0
        for i in range(4):
            lastValue = -1
            for j in range(4):
                if board[i][j] == 0:
                    continue
                elif board[i][j] == lastValue:
                    merges+=1
                    lastValue = 0
                else:
                    lastValue = board[i][j]

        return merges

    def countMergeRight(self, board):
        merges = 0
        for i in range(4):
            lastValue = -1
            for j in range(3,-1,-1):
                if board[i][j] == 0:
                    continue
                elif board[i][j] == lastValue:
                    merges+=1
                    lastValue = 0
                else:
                    lastValue = board[i][j]

        return merges

    def highestTileInCorner(self,board):
        highest = 0
        x = -1
        y = -1
        for i in range(4):
            for j in range(4):
                if board[i][j] > highest:
                    highest = board[i][j]
                    x = i
                    y = j


        cornerList=[0,0,0,0]
        if x == 0:
            if y == 0:
               cornerList[0] = 1
            elif y == 3:
                cornerList[1] = 1
        elif x == 3:
            if y == 0:
                cornerList[2] = 1
            elif y == 3:
                cornerList[3] = 1

        return cornerList