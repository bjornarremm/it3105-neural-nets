#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Bjornars'
    ###################################
    #neural network - Backpropagation##
    ###################################
import theano
import os, struct
from theano import tensor as T
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
import numpy as np
from array import array as pyarray
from mnist_basics import flatten_image
from mnist_basics import load_flat_cases
import sys
import pickle
import math
srng = RandomStreams()


def dropout(X, p=0.):
    if p > 0:
        retain_prob = 1 - p
        X *= srng.binomial(X.shape, p=retain_prob, dtype=theano.config.floatX)
        X /= retain_prob
    return X

def elliot(x,W):
    return 1/2((1+x)/(1+math.fabs(x)))

def sigmoid(X,W):
    return T.nnet.sigmoid(T.dot(X, W))

def sigmoidB(W,x,bias):
    return T.nnet.sigmoid(T.dot(W, x) + bias)

def rectify(X):
    return T.maximum(X, 0.)
    # To fix so you do not get infinity numbers
def softmax(X):
    e_x = T.exp(X - X.max(axis=1).dimshuffle(0, 'x'))
    return e_x / e_x.sum(axis=1).dimshuffle(0, 'x')

def floatX(X):
    return np.asarray(X, dtype=theano.config.floatX)

def init_weights(shape):
    return theano.shared(floatX(np.random.randn(*shape) * 0.01))
class network():

    def __init__(self,syssarn):


       #self.path = Loader('media\datasets\mnist')
                 # size = i.e [784,30,10] which corresponds to number of neurons
        self.size = []
        for i in range(1,len(syssarn)):
            self.size.append(int(syssarn[i]))
        self.numLayers = len(self.size)
        self.weightMatrix=[]
        self.initilizeMatrix()

    def training(self):
        X = T.fmatrix()
        Y = T.fmatrix()
        #This to change number of layers 1, 2 ,3 4 etc


        self.p_drop_input = 0.2
        self.p_drop_hidden = 0.5
        noise = self.model(X,self.weightMatrix,self.numLayers,p_drop_input=self.p_drop_input,p_drop_hidden=self.p_drop_hidden)
        py_x = self.model(X,self.weightMatrix,self.numLayers,0.,0.)
        y_x = T.argmax(py_x,axis=1)
        cost = T.mean(T.nnet.categorical_crossentropy(noise,Y))

        updates = self.RMSprop(cost,self.weightMatrix)

        train = theano.function(inputs=[X, Y], outputs=cost, updates=updates, allow_input_downcast=True, mode="FAST_RUN")
        self.predict = theano.function(inputs=[X], outputs=y_x, allow_input_downcast=True)

        trX, trY = load_flat_cases("all_flat_mnist_training_cases")
        trX,trY = self.preprocess(trX,trY)
        teX,teY = load_flat_cases("all_flat_mnist_testing_cases")
        teX,teY = self.preprocess(teX,teY)
        accuracy=0
        epocs = 0
        print("start")
        while(epocs<5):
            i = 0
            for start, end in zip(range(0, len(trX), 128), range(128, len(trX), 128)):

                self.cost = train([np.reshape(tr, (784)) for tr in trX[start:end]], trY[start:end])
            epocs+=1
        #t = "test"
        #if(t == "test"):
            #self.loadNetwork()
            #print (np.mean(np.argmax(teY, axis=1) == predict(teX)))




    def blind_test(self,flat_cells):
        labelss = np.zeros((len(flat_cells), 10), dtype=np.float)
        for i in range(len(flat_cells)):

            #numbers = [0.0]*10
            flat_cells[i] = np.array(flat_cells[i])/256
            #numbers[int(labels[i])]= 1.0
            #labels[i] = labelss[i]
            #labels[i] = numbers
        return self.predict(flat_cells).tolist()


    def preprocess(self,trDmatrix,labels):
        labelss = np.zeros((len(trDmatrix), 10), dtype=np.float)
        for i in range(len(trDmatrix)):

            numbers = [0.0]*10
            trDmatrix[i] = np.array(trDmatrix[i])/256
            numbers[int(labels[i])]= 1.0
            labels[i] = labelss[i]
            labels[i] = numbers
        return trDmatrix, labels

    def initilizeMatrix(self):
        for i in range(self.numLayers-1):
            self.weightMatrix.append(init_weights((self.size[i],self.size[i+1])))


    def model(self,X, weightmatrix,numberofhidden, p_drop_input, p_drop_hidden):

        xValues =[]
        hidden=[]
        xValues.append(dropout(X,p_drop_input))
        hidden.append(rectify(T.dot(X,weightmatrix[0])))
        #hidden.append(T.tanh(T.dot(X, weightmatrix[0])))


        if(numberofhidden-1)!= 1:
            for i in range(1,numberofhidden-2):
                xValues.append(dropout(hidden[i-1],p_drop_hidden))
                hidden.append(rectify(T.dot(hidden[i-1],weightmatrix[i])))
                #hidden.append(sigmoid(xValues[i-2],weightmatrix[i]))
                #hidden.append(T.tanh(T.dot(xValues[-1], weightmatrix[i])))

            xValues.append(dropout(hidden[len(hidden)-1],p_drop_hidden))


        return softmax(T.dot(xValues[len(xValues)-1],weightmatrix[len(weightmatrix)-1]))
        #softmax(T.dot(xValues[len(xValues)],weightmatrix[numberofhidden]))

    def dropout(X, p=0.):
        if p > 0:
            retain_prob = 1 - p
            X *= srng.binomial(X.shape, p=retain_prob, dtype=theano.config.floatX)
            X /= retain_prob
        return X

    def RMSprop(self,cost, params, lr=0.001, rho=0.9, epsilon=1e-6):
        grads = T.grad(cost=cost, wrt=params)
        updates = []
        for p, g in zip(params, grads):
            acc = theano.shared(p.get_value() * 0.)
            acc_new = rho * acc + (1 - rho) * g ** 2
            gradient_scaling = T.sqrt(acc_new + epsilon)
            g = g / gradient_scaling
            updates.append((acc, acc_new))
            updates.append((p, p - lr * g))
        return updates
