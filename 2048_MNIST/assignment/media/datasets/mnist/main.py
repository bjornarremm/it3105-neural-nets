__author__ = 'Bjornars'

from Network import network
import sys
from mnist_basics import load_flat_cases
from mnist_basics import minor_demo



def main():

 n = network(sys.argv)
 n.training()


 images,labels = load_flat_cases("demo100")
 #n.blind_test(images)
 minor_demo(n)

if __name__ == "__main__": main()